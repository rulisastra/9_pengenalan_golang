package main

import (
	"fmt"
	"net/http"
)

// http://localhost:5000
// yang jalan :D

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "apa kabar")
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "halo")
	}) //jangan lupa ada tanda kurung

	http.HandleFunc("/index", index)

	fmt.Println("Starting webserver")
	http.ListenAndServe(":5000", nil)
}
