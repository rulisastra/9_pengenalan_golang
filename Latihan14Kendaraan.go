package main

import "fmt"

type Kendaraan struct {
	nama  string
	warna string
}

func main() {

	var m = 1000
	var x [1000]Kendaraan
	for j := 0; j < m; j++ {
		var kendaraan Kendaraan
		kendaraan.nama = fmt.Sprint("nama", j)
		kendaraan.warna = fmt.Sprint("warna", j)
		x[j] = kendaraan
	}
	for k := 0; k < m; k++ {
		fmt.Printf("Nama: %s, Warna: %s\n", x[k].nama, x[k].warna)
	}
}
