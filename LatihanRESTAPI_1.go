package main

import (
	"fmt"
	"net/http"
)

// dari bapaknya
// harusnya bisa jalan

// fungsi index (pertama kali dibuka)
func index(w http.ResponseWriter, r *http.Request) { // ini sama kaya parameter. w untuk ResponseWriter dan
	fmt.Fprintln(w, "apa kabar!")					// r untuk Request
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "hello!")
}

func main() {

	http.HandleFunc("/", func(w http.Response, r *http.Request) {
		fmt.Fprintln(w, "holla!!!!!")
	})

	http.HandleFunc("/index", index)

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.ReadRequest) {
		fmt.Fprintln(w, "hello!")
	})

	fmt.Println("Starting webserver at http://localhost:5001")
	http.ListenAndServe(":5001", nil)
}
