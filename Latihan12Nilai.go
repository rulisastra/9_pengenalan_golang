package main

import (
	"fmt"
)

func main() {

	x := 100

	if x <= 100 {
		fmt.Println("A")
	} else if x <= 84 {
		fmt.Println("B")
	} else if x <= 74 {
		fmt.Println("c")
	} else if x <= 59 {
		fmt.Println("D")
	} else {
		fmt.Println("Salah Format")
	}
}
