package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

var m map[string]Vertex

func main() {
	m = make(map[String]Vertex)
	m["Location"] = Vertex{
		40.8264, -74.297634,
	}
	fmt.Println(m["Jakarta"])
}
