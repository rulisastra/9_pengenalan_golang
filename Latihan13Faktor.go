package main

import "fmt"

func main() {

	var n = 20
	for i := 1; i <= n; i++ {
		if n%i == 0 {
			fmt.Printf("%d adalah  faktor dari %d\n", i, n)
		}
	}
}
