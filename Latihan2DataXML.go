package main

import "fmt"

type Gempa struct {
	tanggal                              string
	jam                                  string
	lintang, bujur, magnitude, kedalaman float64
	wilayah                              string
}

var g map[string]Gempa

func main() {
	g = make(map[string]Gempa)
	g["gempa 1"] = Gempa{
		"07-Oct-20", "13:50:13 WIB", 121.11, -10.61, 5.1, 29, "74 km Tenggara WULA-WAIJELU-NTT",
	}
	g["gempa 2"] = Gempa{
		"07-Oct-20", "13:50:13 WIB", 121.11, -10.61, 5.1, 29, "74 km Tenggara WULA-WAIJELU-NTT",
	}
	g["gempa 3"] = Gempa{
		"07-Oct-20", "13:50:13 WIB", 121.11, -10.61, 5.1, 29, "74 km Tenggara WULA-WAIJELU-NTT",
	}
	g["gempa 4"] = Gempa{
		"07-Oct-20", "13:50:13 WIB", 121.11, -10.61, 5.1, 29, "74 km Tenggara WULA-WAIJELU-NTT",
	}
	g["gempa 5"] = Gempa{
		"07-Oct-20", "13:50:13 WIB", 121.11, -10.61, 5.1, 29, "74 km Tenggara WULA-WAIJELU-NTT",
	}

	fmt.Printf("Tanggal: %s\n, Jam: %s\n, Bujur: %f\n, Lintang: %f\n, Magnitude: %f\n, Kedalaman: %f\n, Wilayah: %s\n",
		g["gempa 1"].tanggal,
		g["gempa 1"].jam,
		g["gempa 1"].bujur,
		g["gempa 1"].lintang,
		g["gempa 1"].magnitude,
		g["gempa 1"].kedalaman,
		g["gempa 1"].wilayah)
}
