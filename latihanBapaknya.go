package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

// type Person struct {
// 	nama, umur float64
// }

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func swap(x, y string) (string, string) {
	return x, y
}

func main() {
	fmt.Printf("hari ini %d-%s-%d", 8, "oktober", 2020)
	fmt.Println("halo")

	//panggil swap
	a, b := swap("hello", "world")
	fmt.Printf("var 1: %s, var2: %s", a, b) //ngga bs Println

	// Panggil vertex
	v := Vertex{3, 4}
	fmt.Println(v.Abs())

	// // person
	// var person Person
	// person.nama = "ruli"
	// person.umur = 21
	// fmt.Printf("nama: %s, umur: %s", person.nama, person.umur)

	sum := 0
	for i := 0; i <= 10; i++ {
		sum++
	}
	fmt.Printf("sum: %d", sum)

}
