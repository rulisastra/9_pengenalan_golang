package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type BangunRuang struct {
	Panjang int
	Lebar   int
	Tinggi  int
	Volume  int
	Bentuk  string
}

// hitung volume
func (u BangunRuang) HitungVolume() int {
	return u.Panjang * u.Lebar * u.Tinggi
}

// cek bangun
func (u BangunRuang) CekBangun() string {
	if u.Panjang == u.Lebar && u.Lebar == u.Tinggi {
		return "Kubus"
	} else {
		return "Balok"
	}

}

// hitung luas
func luas(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		var result []byte
		var err error
		var u BangunRuang
		u.Panjang, err = strconv.Atoi(r.FormValue("Panjang"))
		u.Lebar, err = strconv.Atoi(r.FormValue("Tingi"))
		u.Tinggi, err = strconv.Atoi(r.FormValue("Tinggi"))
		u.Bentuk = u.CekBangun()
		u.Volume = u.HitungVolume()
		result, err = json.Marshal(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(result)
	}
	http.Error(w, "", http.StatusBadRequest)
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/luas", luas)
	fmt.Println("Server is running on http://localhost:8080/")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
